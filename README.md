# Spike Report

## GP Spike 1 - Material Editor

### Introduction

The goal of this spike is to learn how to use the Unreal Engine material system and be able to utilise the basics of the material system to help out our artists, and potentially even be able to empower them with new capabilities that they didn�t know we had.

### Goals

1. A base material (M_Basic_PBR) with four parameters  - a Vector parameter for base colour (albedo), a scalar parameter for metallic (default 0), a scalar parameter for specular (default 0.5), and a scalar parameter for roughness (default 0.25).
    - 3+ material instances, including White, Blue, Red, etc. flat colour materials which set the vector parameter, and may touch the scalar parameters for artistic purposes.
    - 3+ material instances for �metallic� objects, including a gold, silver, and bronze.
1. A base material (M_Translucent) using the Translucent blend mode. Same options as the first base material, but with an extra scalar parameter for opacity.
    - 2+ material instances with varying levels of opacity.
    - Use the Shader Complexity view to inspect how much of a performance impact such materials may have
    - Also investigate using the TLM Surface translucency lighting mode
1. A base material (M_Masked) using the Masked blend mode. 
    - 1+ material instances with suitable textured masks (instead of a scalar opacity)
    - Compare and contrast Masked and Translucent blend modes
1. A base material (M_Textured) for fully textured objects utilising PBR correctly (which is hard, as we don�t have an artist we are collaborating with)!
    - Accepts four texture parameters, a base colour map, a PBR map (roughness in one channel, metallic in another channel, specular in another channel), a normal map, and an AO map.
    - 1+ material instances with textures (you can find the textures elsewhere, or you can investigate creating them for certain 3d objects using the Substance Painter or Substance Designer programs)

### Personnel

* Primary - Mitchell Bailey
* Secondary - Bradley Eales

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [PBR Materials Page](https://docs.unrealengine.com/latest/INT/Engine/Rendering/Materials/PhysicallyBased/)
* [Simple Crate 3D Model + Textures](https://www.cgtrader.com/free-3d-models/industrial/other/simple-metallic-crate)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Created empty map, no starter content.

1. Created new material, named it M_Basic_PBR.
    - Attached a vector parameter to the base colour node.
    - Attached a scalar parameter to the metallic, specular, and roughness nodes.
    - Created new material instances called:
        - MI_Basic_Red
        - MI_Basic_White
        - MI_Basic_Blue
    - Adjusted colour, metallic, specular, and roughness parameters to match values given in goals section.
    - Created new material instances called:
        - MI_Basic_Bronze
        - MI_Basic_Silver
        - MI_Basic_Gold
    - Adjusted colour, metallic, specular, and roughness parameters to match values given in goals section.
    - Created 6 spheres in the level and attached a new material instance to each.

1. Created new material, named it M_Translucent.
    - Added same parameters as M_Basic_PBR, but added a scalar parameter to the opacity node.
    - Created new material instances called:
        - MI_Translucent_1
        - MI_Translucent_2
    - Adjusted colour for each instance to differentiate them.
    - Adjusted opacity parameter values for each, 0.25 for MI_Translucent_1 and 0.5 for MI_Translucent_2.
    - Created 2 spheres in the level and attached a new material instance to each.

1. Created new material called M_Masked and changed blend mode setting to Masked.
    - Opened Photoshop and created a simple texture called Masked_Texture.png.
    - Imported texture and attached to Emissive Colour and Opacity Mask using multiply nodes and a vector parameter for colour.
    - Adjusted colour to something easily visible.
    - Created 1 sphere in the level and attached new material instance to it.

1. Created new material called M_Textured.
    - Downloaded Simple Crate 3D Model and Textures from link in previous section.
    - Imported 3D model and textures.
    - Connected BaseColour texture to Base Colour node.
    - Connected Normal texture to Normal node.
    - Connected OcclusionRoughnessMetallic texture to the Occlusion, Roughness, and Metallic nodes, in that order.
    - Created new material instance of M_Textured called MI_Textured.
    - Attached new material instance to 3D crate model.

### What we found out

* A PBR material is a Physically Based Rendering material.
* Learnt how to mask using textures in Unreal.
* Learnt how to texture with base, normal, occlusion, roughness, and metallic textures.